# Project License
This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

## Third-Party Components
1. Arduino OneWire
- Author: Paul Stoffregen
- Project GitHub: [PaulStoffregen/OneWire](https://github.com/PaulStoffregen/OneWire)
- License: Unlisted

2. Arduino DallasTemperature
- Author: Miles Burton
- Project GitHub: [milesburton/Arduino-Temperature-Control-Library](https://github.com/milesburton/Arduino-Temperature-Control-Library)
- License: GNU Lesser General Public License
- Original Project's License: [LICENSE](https://github.com/milesburton/Arduino-Temperature-Control-Library/blob/master/README.md)

3. matplotlib
- Author: Matplotlib Development Team
- Project GitHub: [matplotlib/matplotlib](https://github.com/matplotlib/matplotlib)
- License: Matplotlib License
- Original Project's License: [LICENSE](https://github.com/matplotlib/matplotlib/blob/main/LICENSE/LICENSE)

4. pyserial
- Author: Christopher Bero
- Project GitHub: [pyserial/pyserial](https://github.com/pyserial/pyserial)
- License: BSD 3-Clause License
- Original Project's License: [LICENSE](https://github.com/pyserial/pyserial/blob/master/LICENSE.txt)

5. tkinter
- Author: The Python Software Foundation
- Project GitHub: [python/cpython](https://github.com/python/cpython)
- License: Python Software Foundation License
- Original Project's License: [LICENSE](https://github.com/python/cpython/blob/main/LICENSE)
