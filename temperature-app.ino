/*
 * Arduino Program for Gathering Data from a Temperature Sensor
 *
 * SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Note: The programs and configurations used by this script may not be under the same license.
 * Please check the LICENSING file in the root directory of this repository for more information.
 *
 * This script was created by The Cat Collective.
 */

/*
 * Include Libraries
 */
#include <OneWire.h>
#include <DallasTemperature.h>

/*
 * Pin Layouts
 */
#define ONE_WIRE_BUS 33
#define GREEN_PIN = 25
#define YELLOW_PIN = 26
#define RED_PIN = 27

/*
 * Global Variables
 */
float Celsius = 0;
float Fahrenheit = 0;

/*
 * Initialize the temperature probe
 */
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

/*
 * Setup Function
 */
void setup(void) {
    pinMode(RED_PIN, OUTPUT);
    pinMode(YELLOW_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    Serial.begin(112500);
    sensors.begin();
}

/*
 * Loop Function
 */
void loop(void) {
    sensors.requestTemperatures();
    Celcius = sensors.getTempCByIndex(0);
    Fahrenheit = sensors.toFahrenheit(Celcius);

    if (Fahrenheit < 30 || Fahrenheit > 120) {
        digitalWrite(RED_PIN, HIGH);
        delay(250);
        digitalWrite(RED_PIN, LOW);
        Serial.println(Fahrenheit);
    }
    else if ((Fahrenheit > 30 && Fahrenheit <= 60) || (Fahrenheit > 90 && Fahrenheit <= 120)) {
        digitalWrite(YELLOW_PIN, HIGH);
        delay(250);
        digitalWrite(YELLOW_PIN, LOW);
        Serial.println(Fahrenheit);
    }
    else if (Fahrenheit > 60 && Fahrenheit <= 90) {
        digitalWrite(GREEN_PIN, HIGH);
        delay(250);
        digitalWrite(GREEN_PIN, LOW);
        Serial.println(Fahrenheit);
    }
}
