#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Graphing Program for COM Port Input Data
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

import serial
import tkinter as tk
from tkinter import ttk, simpledialog
from typing import Optional

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class TemperatureApp:
    """
    Main application class for the temperature monitoring system.

    Attributes:
        root (tk.Tk): The root Tkinter window.
        ser (Optional[serial.Serial]): The serial communication object.
        plot_update_scheduled (bool): Flag to track whether the plot update loop is scheduled.
    """

    def __init__(self, root: tk.Tk):
        """
        Initialize the TemperatureApp.

        Parameters:
            root (tk.Tk): The root Tkinter window.
        """
        self.root: tk.Tk = root
        self.root.title('Python App')

        # Initialize the serial object with a placeholder port
        self.ser: Optional[serial.Serial] = None

        # Variable to track whether the plot update loop is scheduled
        self.plot_update_scheduled: bool = False

        self.create_widgets()

    def create_widgets(self) -> None:
        """
        Create and layout the GUI widgets.
        """
        self.current_temp_label: ttk.Label = ttk.Label(self.root, text='Current Temp.')
        self.current_temp_label.grid(row=0, column=0, padx=10, pady=10, sticky='e')

        self.current_temp_entry: ttk.Entry = ttk.Entry(self.root, width=10, state='readonly')
        self.current_temp_entry.grid(row=0, column=1, padx=10, pady=10, sticky='w')

        self.press_to_begin_button: ttk.Button = ttk.Button(self.root, text='Press to begin', command=self.press_to_begin)
        self.press_to_begin_button.grid(row=1, column=0, columnspan=2, pady=10)
        self.figure: Figure = Figure(figsize=(5, 4), dpi=100)
        self.ax = self.figure.add_subplot(111)
        self.canvas: FigureCanvasTkAgg = FigureCanvasTkAgg(self.figure, master=self.root)
        self.canvas_widget: tk.Widget = self.canvas.get_tk_widget()
        self.canvas_widget.grid(row=2, column=0, columnspan=2, padx=10, pady=10)

    def press_to_begin(self) -> None:
        """
        Handler for the "Press to begin" button. Opens a dialog for the user to input the COM port.
        """
        com_port: Optional[str] = simpledialog.askstring('COM Port', 'Enter the COM port (e.g., COM3):')
        if com_port:
            self.initialize_serial(com_port)

    def initialize_serial(self, com_port: str) -> None:
        """
        Initialize the serial communication with the specified COM port.

        Parameters:
            com_port (str): The COM port identifier (e.g., COM3).
        """
        try:
            # Close the existing serial connection if it exists
            if self.ser:
                self.ser.close()

            # Initialize the serial object with the user-provided COM port
            self.ser = serial.Serial(com_port, 115200, timeout=1)

            # Set up the after method to continuously update the plot
            if not self.plot_update_scheduled:
                self.root.after(100, self.update_plot)
                self.plot_update_scheduled = True
        except serial.SerialException:
            print(f"Error: Unable to open serial port {com_port}.")

    def update_plot(self) -> None:
        """
        Update the plot with temperature data received from the serial port.
        """
        try:
            if self.ser:
                line: str = self.ser.readline().decode('utf-8').strip()
                if line:
                    p: float = float(line)
                    self.update_temperature_entry(p)
                    self.update_plot_data(p)
        except serial.SerialException:
            print('Error: Unable to read from serial port.')

        # Schedule the next update
        self.root.after(100, self.update_plot)

        # Check if no data has been received for more than 1 second
        if self.ser and self.ser.in_waiting == 0:
            print('No data received within the timeout.')
            self.ser.close()

    def update_temperature_entry(self, temperature: float) -> None:
        """
        Update the temperature entry widget with the latest temperature value.

        Parameters:
            temperature (float): The temperature value.
        """
        self.current_temp_entry.delete(0, tk.END)
        self.current_temp_entry.insert(0, f"{temperature:.2f}")

    def update_plot_data(self, temperature: float) -> None:
        """
        Update the plot data with the latest temperature value.

        Parameters:
            temperature (float): The temperature value.
        """
        x = list(self.ax.lines[0].get_xdata())
        y = list(self.ax.lines[0].get_ydata())
        x.append(x[-1] + 1)
        y.append(temperature)

        if len(x) > 200:
            x = x[1:]
            y = y[1:]

        self.ax.clear()
        self.ax.plot(x, y)
        self.ax.set_xlim([min(x), max(x)])
        self.ax.set_ylim([min(y), max(y)])
        self.ax.set_title('Arduino Temperature Sensor')
        self.ax.set_xlabel('Time (s)')
        self.ax.set_ylabel('Temperature (F)')
        self.canvas.draw()

if __name__ == '__main__':
    root: tk.Tk = tk.Tk()
    app: TemperatureApp = TemperatureApp(root)
    root.mainloop()
