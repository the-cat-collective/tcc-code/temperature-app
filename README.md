# Temperature App

## Project Information
Temperature App was a project for our University's Introduction to Systems Engineering course. The program to display the graphs was originally written in MATLAB but has since been ported to Python.

## Getting Started
### Physical Dependencies
* An Arduino or Arduino-compatible device
* A temperature sensor for the Arduino
* Arduino jumper cables
* Coloured lights

### Software Dependencies
Download the dependencies for the project and then clone the repo.
This project requires:
```bash
git
python3
matplotlib
pyserial
OneWire.h
DallasTemperature.h
```
To install the Python dependencies, please ensure you have `python3.6` or newer installed and then run the following command:
```bash
$ pip install maplotlib pyserial
```
This project also utilizes `pre-commit` for static typechecking but does not directly depend upon it. To install `pre-commit`, please ensure you have `python3.6` or newer installed and then run the following command:
```bash
$ pip install pre-commit
$ pre-commit install
```
To verify for yourself that this code uses the proper type annotations, run the following command after installing `pre-commit` and cloning the repo as (described below):
```bash
$ pre-commit run --all-files
```
As far as the C libraries are concerned, they can be downloaded from ArduinoIDE's Library Manager. Once you download them, use ArduinoIDE to compile the project file and flash it to your Arduino.

## Cloning the repo
Run the following command to clone the sources:
```bash
$ git clone https://gitlab.com/the-cat-collective/tcc-code/temperature-app.git
```

## Project Structure
The root of the project contains the `visualizer.py` script which listens on COM ports for a data connection from an Arduino or Arduino-compatible device and displays the temperature/time graph on screen. The root of the project also contains the `README` and `LICENSE` files.

## Example usage
### Configuring Jumper Cable Pin Connections
To adapt the project to your specific Arduino board and sensor setup, you may need to adjust the jumper cable pin connections in the `temperature-app.ino` file. Open the `temperature-app.ino` file in your preferred code editor and locate the section where the pin configurations are defined. The comments within the file should guide you on which pins are used for connecting the temperature sensor and other components.

Make sure to review and edit the pin configurations to match your specific Arduino board and sensor layout. If you are unsure about the pin connections, refer to your Arduino board's documentation or the datasheets for the components you are using.

### Actually getting started
Compile and upload the `temperature-app.ino` file to your Arduino, make sure you have a COM connection, and then place your Temperature Sensor in different environments and watch the data get displayed on the graph and the colour of the lights change!

## Licensing
* This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

* For licensing information related to first and third-party components used in this project, please refer to the [LICENSING](LICENSING.md) file.

## Disclaimer
None
